import java.util.Scanner; // Necesaria para leer la entrada del usuario

/**
 *
 * @author
 */
public class dbclient {
    public static Scanner scan = new Scanner(System.in);
    public static Database db;
    //static int currentRow; // Row currently containing the disturbance.
    //static int currentColumn; // Column currently containing disturbance.

    /**
     * @param args the command line arguments
     * @param file Ruta al archivo donde se encuentra la base de datos
     * @param size Entero que determina el número de registros que puede tener la base de datos
     */
    public static void main(String[] args) {
        validarArgumentos(args);
        db = new Database(args[0], Integer.parseInt(args[1]));
        
        while(esperarComando());
        System.out.println("Gracias por usar el GestorBD");
        System.exit(0);
    }
    
    /**
     * Valida que los parametros iniciales de la función main() se encuentren presentes y sean del tipo de datos correcto
     * @param args los argumentos de la linea de comando
     */
    static void validarArgumentos(String[] args) {
        int firstArg, secondArg;
        if (args.length > 1) { // Checa si la cantidad de argumentos proporcionados es mayor a 1 (tienen que ser 2 para que el programa pueda continuar)
            try {
                secondArg = Integer.parseInt(args[1]); // Se hace la conversión del segundo parametro a un entero
            } catch (NumberFormatException e) { // Si la conversión no fue exitosa entonces el argumento es un tipo inválido, osea no es número
                System.err.println("Debes proporcionar un número válido como segundo argumento");
                System.exit(1); // Termina el programa
            }
            // System.out.println("Tu BD se llamará: " + args[0]);
        } else { // Si la cantidad de argumentos no es mayor a 1 entonces termina el programa
            System.err.println("No se han proporcionado los argumentos correctos");
            System.exit(1); // Termina el programa
        }
    }
    
    static boolean esperarComando(){
        System.out.print("Escribe el comando: ");
        String cmd;
        cmd = scan.nextLine();
                
        return seleccionarComando(cmd);
    }
    
    static boolean seleccionarComando(String cmd){
        db.startTimer();                                                         // Se comienza a medir el tiempo de ejecucion del query
 
        if (cmd == null || cmd.isEmpty()) {                                     // Verifica si se escribio un comando
            System.out.println("Por favor escribe un comando");
            return true;
        }
        
        String[] command = cmd.split(" ");
        
        if (command.length > 2) {
            System.out.println("La cantidad de argumentos es invalida");
            return true;
        }
        
        if (command.length == 1) {
            switch(command[0]){
                case "SAVE":
                    db.save();
                    return true;
                case "EXIT":
                    exit();
                default:
                    System.out.println("Comando invalido");
                    return true;
            }
        }
        
        switch(command[0]){
            case "INSERT":
                db.insert(command[1]);
                return true;
            case "DELETE":
                db.delete(command[1]);
                return true;
            case "SELECT":
                db.select(command[1]);
                return true;
            default:
                //System.out.println("Comando invalido");
                return true;
        }        
    }
    
    static void exit(){
        System.out.println("Gracias por usar el GestorBD");
        System.exit(0);
    }
}
