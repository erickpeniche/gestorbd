public class Record {
    public int    index;
    public String value;
    
    public Record(int index, String value) {
        this.index = index;
        this.value = value;
    }
}
