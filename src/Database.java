/**
 *
 * @author 
 */

import java.io.File;                                                            // Librerias que se encargan de manipular entradas y salidas de ficheros
import java.io.IOException;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.util.Scanner;                                                       // Nos sirve para leer el archivo de texto
import java.util.List;
import java.util.ArrayList;
import java.util.Date;                                                          // Nos sirve para comparar el tiempo que transcurre en realizar el query
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.Arrays;

public class Database {
    
    // Declaramos propiedades privadas de la clase Database
    private String   filename; // Nombre del archivo
    private File     database; // Archivo
    private Scanner  reader; // Objeto scanner para leer el archivo
    private Record[] data; // Objeto para almacenar los datos introducidos por el usuario
    private Record[] sortedData;
    private int      maximumSize;
    private int      numberOfRecords;
    private Date     startTime, endTime;
    
    public Database(String file, int size){                                     // Constructor de la clase
        this.filename        = file + ".txt";
        this.maximumSize     = size;
        this.data            = new Record[this.maximumSize];
        this.sortedData      = new Record[this.maximumSize];
        this.numberOfRecords = 0;
        
        this.loadDatabase(this.filename);
    }
    
    private void loadDatabase(String filePath){
        this.database = new File(filePath);

        if (this.database.exists() && !this.database.isDirectory()) {           // El archivo existe, entonces leerlo
            this.readFile();
            this.sortData();
        } else {                                                                // Si no existe entonces se crea el nuevo archivo
            try { 
                // Se prueba crear el archivo
                this.database.createNewFile();
            } catch (IOException ex) {                                          // Si no se pudo crear, muestra error
                System.err.println("No se ha podido crear la nueva base de datos");
            }
        }
    }
    
    private void readFile(){
        this.initScanner();
        
        String line;
        
        while (this.reader.hasNextLine()) {
            line = this.reader.nextLine();
            
            if (line.isEmpty()) {
                continue;
            }

            this.insertRecord(line, false);
        }

        this.reader.close();
    }
    
    private void insertRecord(String value, boolean sort) {
        int index     = ++this.numberOfRecords;
        Record record = new Record(index, value);
        
        this.data[index - 1]       = record;
        this.sortedData[index - 1] = record;
        
        if (sort == true) {
            this.sortData();   
        }
    }
    
    private void sortData() {
        Record temporary;
        
        // Bubble sort
        for (int i = 0; i < this.numberOfRecords; i++) {
            for (int j = 1; j < this.numberOfRecords - i; j++) {
                if (this.sortedData[j - 1].value.compareTo(this.sortedData[j].value) > 0) {
                    temporary              = this.sortedData[j - 1];
                    this.sortedData[j - 1] = this.sortedData[j];
                    this.sortedData[j]     = temporary;
                }
            }
        }   
    }
    
    private void initScanner(){
        try { // Se prueba crear el scanner para leer el archivo
            this.reader = new Scanner(this.database);
        } catch (IOException ex) { // Si no se pudo abrir, muestra error
            System.err.println("No se ha podido abrir el archivo");
            System.exit(1); // Termina el programa
        }
    }
    
    public void select(String value) {
        List<Record> foundRecords = this.search(value);
        
        if (foundRecords.isEmpty()) {
            System.out.println("> Records not found in " + this.getElapsedTime() + "s");   
            return;
        }
        
        String[] positions = new String[foundRecords.size()];
        int lastUsedIndex  = -1; 
        
        for (Record record : foundRecords) {
            positions[++lastUsedIndex] = Integer.toString(record.index);
        }
        
        Arrays.sort(positions);
        
        System.out.println("> SELECTED records " + this.combine(positions, ",") + " in " + this.getElapsedTime() + "s");   
    }
    
    private String combine(String[] s, String glue)
    {
      int k=s.length;
      if (k==0)
        return null;
      StringBuilder out=new StringBuilder();
      out.append(s[0]);
      for (int x=1;x<k;++x)
        out.append(glue).append(s[x]);
      return out.toString();
    }
    
    private List<Record> search(String value) {
        List<Record> foundRecords = new ArrayList<Record>();

        if (this.numberOfRecords == 0) {
            return foundRecords;
        }

        int low  = 0;
        int high = this.numberOfRecords - 1;

        while (low <= high) {
            int middle = (low + high) / 2; 

            if (value.compareTo(this.sortedData[middle].value) > 0){
                low = middle + 1;
            } else if (value.compareTo(this.sortedData[middle].value) < 0){
                high = middle - 1;
            } else {
                foundRecords.add(this.sortedData[middle]);
                this.searchLeftOccurrences(middle, value, foundRecords);
                this.searchRightOccurrences(middle, value, foundRecords);
                
                break;
            }
        }
        
        return foundRecords;
    }
    
    private void searchLeftOccurrences(int middle, String value, List<Record> foundRecords) {
        for (int i = middle - 1; i >= 0; i--) {
            if (this.sortedData[i].value.compareTo(value) != 0) {
                break;
            }

            foundRecords.add(this.sortedData[i]);                    
        }
    }
    
    private void searchRightOccurrences(int middle, String value, List<Record> foundRecords) {
        for (int i = middle + 1; i < this.numberOfRecords; i++) { 
            if (this.sortedData[i].value.compareTo(value) != 0) {
                break;
            }

            foundRecords.add(this.sortedData[i]);
        } 
    }
    
    public void delete(String value) {
        List<Record> foundRecords = this.search(value);
        
        for (Record record : foundRecords) {
            this.deleteRecord(record.index);
        }
       
        this.rearrangeData();
        this.reloadSortedData();
        
        System.out.println("> DELETED records " + foundRecords.size() + " in " + this.getElapsedTime() + "s");
    }
    
    private void rearrangeData() {
        Record[] temporalData = new Record[this.maximumSize];
        System.arraycopy(this.data, 0, temporalData, 0, this.maximumSize);
        
        this.data = new Record[this.maximumSize];
        
        int lastUsedIndex = -1;
        
        for (int i = 0; i < this.maximumSize; i++) {
            if (temporalData[i] == null) {
                continue;
            }
            
            ++lastUsedIndex;
            
            temporalData[i].index = lastUsedIndex + 1;
            this.data[lastUsedIndex] = temporalData[i];
        }
    }
    
    
    private void reloadSortedData() {
        this.sortedData = new Record[this.maximumSize];
        System.arraycopy(this.data, 0, this.sortedData, 0, this.maximumSize);
        this.sortData();
    }
    
    private void deleteRecord(int index) {
        this.data[index - 1] = null;
        this.numberOfRecords--;
    }
    
    public void printDatabase(){
        System.out.println("La base de datos contiene las siguientes entradas: \n");
        for (Record word: this.data) {
            System.out.println(word); //Imprime el valor de cada espacio en el arreglo
        }
    }
    
    public String getFilename(){ // Método para devolver el nombre del archivo
        return this.filename;
    }
    
    public int getMaximumSize(){ // Método para devolver el tamaño de la base de datos
        return this.maximumSize;
    }
    
    public void insert(String query){
        if (this.numberOfRecords == this.maximumSize) {
            System.out.println("No slots available");
        } else {
            this.insertRecord(query, true);
            System.out.println("> Inserted the word " + query + " in " + this.getElapsedTime() + "s");
        }
    }
    
    public void save(){
        if (!this.database.delete()) {
            System.err.println("No se ha podido eliminar el archivo");
        }
        
        this.loadDatabase(this.filename);
        
        BufferedWriter writer = null ; 
        try {
            writer = new BufferedWriter(new FileWriter(this.filename)); //you don't need to create a File object, FileWriter takes a string for the filepath as well
        } catch (IOException ex) {
            System.err.println("No se ha podido escribir");
        }
        
        for(Record entry: this.data){
            if (entry == null) {
                continue;
            }
            try {
                writer.write(entry.value);
            } catch (IOException ex) {
                System.err.println("No se ha pudo escribir el valor");
            }
            try {
                writer.newLine();
            } catch (IOException ex) {
                System.err.println("No se ha pudo agregar la nueva linea");
            }
        }
        System.out.println("SAVED db in " + this.getElapsedTime() + "s");
        try {
            writer.close();
        } catch (IOException ex) {
            Logger.getLogger(Database.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void startTimer(){
        this.startTime = new Date();
    }
    
    public float getElapsedTime(){
        this.endTime = new Date();
        //System.out.println("End: " + this.endTime.getTime());
        return (float) (( this.endTime.getTime() -  this.startTime.getTime())/ 100.00);
    }
}
